﻿using InventoryCRUD.Data;
using InventoryCRUD.Interfaces;
using InventoryCRUD.Models;
using Microsoft.EntityFrameworkCore;

namespace InventoryCRUD.Repository
{
    public class UnitRepository : IUnit
    {
        private readonly InventoryContext _context;

        /* real only variables can be set only once so it assings reassigning the value mistakenly */

        public UnitRepository(InventoryContext context)
        {
            _context = context;
            /* value is passed here with dependency injection */
        }

        public Unit Create(Unit unit)
        {
            _context.Units.Add(unit);
            _context.SaveChanges();
            return unit;
        }

        public Unit Delete(Unit unit)
        {
            _context.Units.Attach(unit);
            _context.Entry(unit).State = EntityState.Deleted;
            _context.SaveChanges();
            return unit;
        }

        public Unit Edit(Unit unit)
        {
            _context.Units.Attach(unit);
            _context.Entry(unit).State = EntityState.Modified;
            _context.SaveChanges();
            return unit;
        }

        public List<Unit> GetItems()
        {
            List<Unit> units = _context.Units.ToList();
            return units;
        }

        public Unit GetUnit(int id)
        {
            Unit unit = _context.Units.Where(u => u.Id == id).FirstOrDefault();
            return unit;
        }

      
    }
}
