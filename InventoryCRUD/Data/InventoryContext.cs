﻿using InventoryCRUD.Models;
using Microsoft.EntityFrameworkCore;

namespace InventoryCRUD.Data
{
    public class InventoryContext : DbContext
    {
        public InventoryContext(DbContextOptions options) : base(options)
        {
            /* the options paramter will carry connection string in our case*/
        }

        /* We use DbContext class to interact with the underlying database, it manages db conn
         * and is used to retrieve and send data to the database. For Dbcontext class to do any
         useful operation it needs an instance of DbContext class. We need to pass the 
         parameter of the DbContextOptions class to the base DbContext Class*/

        public DbSet<Unit> Units { get; set; }
        
        /* We need a table name units in our database with the properties defined in the unit
         * class here, so whatever model we have to creat inside the db, we will have to create a DB
         set inside this class */
    }
}
