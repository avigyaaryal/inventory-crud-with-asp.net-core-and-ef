﻿using InventoryCRUD.Data;
using InventoryCRUD.Interfaces;
using InventoryCRUD.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace InventoryCRUD.Controllers
{
    public class UnitController : Controller
    {
        /* read method of the crdu operations. It lists all the data from the units table */
        public IActionResult Index()
        {
            List<Unit> units = _unitRepo.GetItems(); //_context.Units.ToList();
            return View(units);
        }

        //readonly variable can be set only once so it avoids resigning the value mistakenly 

        private readonly IUnit _unitRepo;

        public UnitController(IUnit unitrepo)
        {
            _unitRepo = unitrepo;
        }

        //view belongs in presentation layer

        public IActionResult Create()
        {
            Unit unit = new Unit();
            return View(unit);

            /* loads the view file for the user to enter the unit details  */
        }

        [HttpPost]
        public IActionResult Create(Unit unit)
        {
            try
            {
                unit = _unitRepo.Create(unit);
            }
            catch
            {

            }
            

            return RedirectToAction(nameof(Index));

            /* this will pass the controller to the index method and we will be redirected to the
             * listing page of the units */
        }

        public IActionResult Details(int id)
        {
            Unit unit = _unitRepo.GetUnit(id);
            return View(unit);
        }

        public IActionResult Edit(int id)
        {
            Unit unit = _unitRepo.GetUnit(id);
            return View(unit);
        }

        [HttpPost]
        public IActionResult Edit(Unit unit)
        {
            try
            {
                unit = _unitRepo.Edit(unit);
            }
            catch
            {

            }
            

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Delete(int id)
        {
            Unit unit = _unitRepo.GetUnit(id);
            return View(unit);
        }

        [HttpPost]
        public IActionResult Delete(Unit unit)
        {
            try
            {
                unit = _unitRepo.Delete(unit);
            }
            catch
            {

            }


            return RedirectToAction(nameof(Index));
        }

        

    }
}
