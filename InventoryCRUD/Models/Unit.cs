﻿using System.ComponentModel.DataAnnotations;

namespace InventoryCRUD.Models
{
    public class Unit
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }
        
        /* Entity Framework is an ORM - Object Relational Mapper which stands between the business
         * layer and the underlying database. To do all the database related operations it requires a 
         * important class of type DbContext */
    }
}
